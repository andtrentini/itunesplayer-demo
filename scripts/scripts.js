var corsProxy = 'https://cors-anywhere.herokuapp.com/';

function cercaArtisti() {
  var artistName = document.getElementById('search-text').value;
  var url = 'https://itunes.apple.com/search?term=' + artistName + '&entity=musicArtist&callback=?';

  $.getJSON(url, (artisti) => {
    artisti = artisti.results;
    creaListaArtisti(artisti);
  });
}

function cercaArtistiXHR() {
  // https://itunes.apple.com/search?parameterkeyvalue


  var artistName = document.getElementById('search-text').value;

  client.onreadystatechange = () => {
    console.log(client.getAllResponseHeaders());
    if (client.readyState == 4 && client.status == 200) {
      var artisti = JSON.parse(client.responseText);
      artisti = artisti.results;
      creaListaArtisti(artisti);
    }
  }

  client.open('GET', corsProxy + 'https://itunes.apple.com/search?term=' + artistName + '&entity=musicArtist');
  client.send();

}

function caricaAlbumArtistaXHR(idArtista) {
  // https://itunes.apple.com/lookup?id=909253&entity=album

  var client = new XMLHttpRequest();

  client.onreadystatechange = () => {
    if (client.readyState == 4 && client.status == 200) {
      var listaAlbum = JSON.parse(client.responseText);
      listaAlbum = listaAlbum.results;
      creaAlbumGrid(listaAlbum);
    }
  }

  client.open('GET', corsProxy + 'https://itunes.apple.com/lookup?entity=album&id=' + idArtista);

  client.send();
}

function caricaAlbumArtista(idArtista) {
  // https://itunes.apple.com/lookup?id=909253&entity=album

  var url = 'https://itunes.apple.com/lookup?id=' + idArtista + '&entity=album&callback=?';

  $.getJSON(url, (listaAlbum) => {
    listaAlbum = listaAlbum.results;
    creaAlbumGrid(listaAlbum);
  });
}


function caricaTracksAlbum(idAlbum) {
  // https://itunes.apple.com/lookup?idCollection=909253

  var url = 'https://itunes.apple.com/lookup?id=' + idAlbum + '&limit=200&entity=song&callback=?';

  $.getJSON(url, (listaTracce) => {
    listaTracce = listaTracce.results;
    var results = document.getElementById('results');
    results.innerHTML = '';
    results.appendChild(creaTrackGrid(listaTracce));
  });
}

function caricaTracksAlbumHXR(idAlbum) {
  // https://itunes.apple.com/lookup?idCollection=909253

  var client = new XMLHttpRequest();

  client.onreadystatechange = () => {
    if (client.readyState == 4 && client.status == 200) {
      var listaTracce = JSON.parse(client.responseText);
      listaTracce = listaTracce.results;

      var results = document.getElementById('results');
      results.innerHTML = '';
      results.appendChild(creaTrackGrid(listaTracce));
    }
  }

  client.open('GET', corsProxy + 'https://itunes.apple.com/lookup?limit=200&entity=song&id=' + idAlbum);

  client.send();
}

function creaListaArtisti(artisti) {
  document.getElementById('results').innerHTML = '';
  var lista = document.createElement('div');
  lista.className = 'list-group';

  artisti.forEach(artista => {
    var bottone = document.createElement('button');
    bottone.type = 'button';
    bottone.classList = 'list-group-item list-group-item-action';
    bottone.innerText = artista.artistName;
    bottone.onclick = () => {
      // Codice da eseguire al click sul bottone
      caricaAlbumArtista(artista.artistId)
    }
    lista.appendChild(bottone);
  });

  document.getElementById('results').appendChild(lista);
}

function creaAlbumCard(album) {
  /*
<div class="card">
  <img src="..." class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>    
  </div>
</div>
*/

  var card = document.createElement('div');
  card.className = 'card';

  var img = document.createElement('img');
  img.src = album.artworkUrl100;
  img.className = 'card-img-top';
  img.alt = album.collectionName;
  img.onclick = () => {
    caricaTracksAlbum(album.collectionId);
  }

  card.appendChild(img);

  var cardBody = document.createElement('div');
  cardBody.className = 'card-body';

  var titolo = document.createElement('h5');
  titolo.className = 'card-title';
  titolo.innerText = album.collectionName;

  cardBody.appendChild(titolo);

  var text = document.createElement('p');
  text.className = 'card-text';
  text.innerHTML = album.artistName + '<br/>' + new Date(album.releaseDate).getFullYear() + '<br />' +
    album.trackCount + ' tracce';

  cardBody.appendChild(text);

  card.appendChild(cardBody);

  return card;
}

function getArtistaName(artista) {
  var riga = document.createElement('div');
  riga.className = 'row';
  var tagNome = document.createElement('h2');
  tagNome.className = 'display-4';
  tagNome.innerText = artista.artistName;
  riga.appendChild(tagNome);
  return riga;
}

function creaAlbumGrid(elencoAlbum) {

  // Recupero il primo elemento dell'array che contiene i dati dell'artista
  var artista = elencoAlbum[0];

  // Cancello dall'array i dati dell'artista e recupero solo l'elenco degli album
  var albums = elencoAlbum.slice(1);
  /*
  <div class="row">
      <div class="col-sm-4">
        inserire card
      </div>
      <div class="col-sm-4">
        inserire card
      </div>
      <div class="col-sm-4">
        inserire card
      </div>
      <div class="col-sm-4">
        inserire card
      </div>
  */


  var griglia = document.createElement('div');
  griglia.className = 'row';

  albums.forEach(album => {
    var cella = document.createElement('div');
    cella.classList = 'col-md-6 col-lg-4 mb-4';
    cella.appendChild(creaAlbumCard(album));
    griglia.appendChild(cella);
  });

  var results = document.getElementById('results');
  results.innerHTML = '';

  // Visualizza i dati dell'artista
  results.appendChild(getArtistaName(artista));

  // Visualizza la griglia di album
  results.appendChild(griglia);

}

function creaTrackGrid(elencoTracce) {
  var album = elencoTracce[0];
  var tracce = elencoTracce.slice(1);

  var vista = document.createElement('div');
  vista.appendChild(getArtistaName(album));
  var griglia = document.createElement('div');
  griglia.className = 'row';

  var sinistra = document.createElement('div');
  sinistra.className = 'col-sm-4';
  sinistra.appendChild(creaAlbumCard(album));

  griglia.appendChild(sinistra);

  var destra = document.createElement('div');
  destra.className = 'col-sm-8';
  destra.appendChild(creaTabellaTracce(tracce));

  griglia.appendChild(destra);
  vista.appendChild(griglia);

  return vista;
}

function creaTabellaTracce(tracce) {
  /*
      <table class="table table-sm">
      ...
      </table>
  */

  var tabella = document.createElement('table');
  tabella.classList = 'table table-striped table-sm border';
  var header = document.createElement('thead');
  header.appendChild(creaIntestazioneTabella());
  tabella.appendChild(header);
  var body = document.createElement('tbody');
  tracce.forEach(traccia => {
    body.appendChild(creaRigaTraccia(traccia));
  });
  tabella.appendChild(body);

  return tabella;
}

function creaIntestazioneTabella() {
  var riga = document.createElement('tr');
  var cella = document.createElement('th');
  cella.innerText = '#';
  riga.appendChild(cella);
  cella = document.createElement('th'); // cancellare l'struzione var
  cella.innerText = 'Titolo';
  riga.appendChild(cella);
  cella = document.createElement('th'); // cancellare l'struzione var
  cella.innerText = 'Durata';
  riga.appendChild(cella);
  cella = document.createElement('th'); // cancellare l'struzione var
  cella.innerText = '';
  riga.appendChild(cella);

  return riga;
}

function creaRigaTraccia(traccia) {
  var riga = document.createElement('tr');
  var cella = document.createElement('td');
  cella.innerText = traccia.trackNumber;
  riga.appendChild(cella);
  cella = document.createElement('td'); // cancellare l'struzione var
  cella.innerText = traccia.trackName;
  riga.appendChild(cella);
  cella = document.createElement('td'); // cancellare l'struzione var
  cella.innerText = convertMillisec(traccia.trackTimeMillis);
  riga.appendChild(cella);
  cella = document.createElement('td'); // cancellare l'struzione var
  cella.innerText = 'play';
  riga.appendChild(cella);

  return riga;
}

function convertMillisec(millisec) {
  var time = new Date(millisec);
  if (time.getSeconds() < 10) {
    return time.getMinutes() + ':0' + time.getSeconds();
  } else {
    return time.getMinutes() + ':' + time.getSeconds();
  }
}